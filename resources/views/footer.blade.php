<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg">
                <div class="row">
                    <div class="col-12 col-lg-12 col-md-12">
                        <h5>Contacto</h5>
                    </div>
                    <div class="col-12 col-lg-12 col-md-12">
                      <p>Telefono: 2343232</p>
                        <p>Direccion: Enrique Segoviano</p>
                    </div>
                    
                </div>                   
            </div>
            <div class="col-12 col-lg">
                <a href="#">Preguntas Frecuentes</a>
            </div>
            <div class="col-12 col-lg">
                <a href="#">Terminos y condiciones</a>
            </div>
        </div>
    </div>
</footer>
<div id="footerDivider" class="pt-3">  
   <div class="container">
       <div class="row">
           <div class="col-lg-6 col-12">
               <p>Copyright @2021 TecnoEduca</p>
           </div>
           <div class="col-lg-6 col-12 d-flex justify-content-end">
            <a href="#">Noticias de privacidad</a>
        </div>
       </div>      
   </div> 
</div>